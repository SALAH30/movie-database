"""
Main script for testing queries
"""
import os
import sys

from globals.globals import (
    DATABASE_DIR_PATH,
    TESTS_RESOURCES_DIR_PATH,
)
from models.api import DatabaseApi
from models.query import Query
from tests.builder import TestsBuilder
from tests.fixtures import JSON_FILE_PATHS
from tests.validator import Validator
from utils.json_utils import JSONUtils
from utils.logger import logger


def run_test(
    expected_result_file_path: str,
    name: str,
    your_result: dict,
) -> None:
    """
    Run a test

    :param str expected_result_file_path: The path to the JSON file whose content is the expected result
    :param str name: The name of the test
    :param dict your_result: Your result to compare with the expected one

    :return: None
    """
    try:
        expected_result = JSONUtils.read_json(file_path=expected_result_file_path)
        assert your_result == expected_result
        logger.info(f"✅ TEST {name}: VALIDATED")
    except AssertionError:
        error_msg = f"❌ TEST {name}: FAILED\n"
        error_msg += Validator.diff_dict(
            ref_dict=expected_result,
            comp_dict=your_result,
        )
        error_msg += f"\n-> See file {expected_result_file_path}"
        logger.error(error_msg)

def my_tests() -> None:
    """
    Test queries on JSON database
    """
    # Build test resources
    TestsBuilder.dump_fixture(
        fixture=JSON_FILE_PATHS,
        file_path=os.path.join(TESTS_RESOURCES_DIR_PATH, "json_file_paths.json"),
    )
    logger.info(f"✅ TEST resources have been built")
    # Create DatabaseApi object to browse the JSON database
    database_api = DatabaseApi(database_dir_path=DATABASE_DIR_PATH)
    # Complete the classes DatabaseApi and Query to pass the following tests
    # Getter models.api.DatabaseApi.json_file_paths
    test_name = "models.api.DatabaseApi.json_file_paths 1/1"
    try:
        run_test(
            expected_result_file_path=os.path.join(TESTS_RESOURCES_DIR_PATH, "api_json_file_paths.json"),
            name=test_name,
            your_result=database_api.json_file_paths,
        )
    except NotImplementedError:
        logger.warning(f"🕒 TEST {test_name}: SOLUTION NOT IMPLEMENTED YET")
    # Getter models.api.DatabaseApi.database
    test_name = "models.api.DatabaseApi.database 1/1"
    try:
        run_test(
            expected_result_file_path=os.path.join(TESTS_RESOURCES_DIR_PATH, "api_database.json"),
            name=test_name,
            your_result=database_api.database,
        )
    except NotImplementedError:
        logger.warning(f"🕒 TEST {test_name}: SOLUTION NOT IMPLEMENTED YET")
    # Getter models.query.Query.inventory
    test_name = "models.query.Query.inventory 1/1"
    try:
        run_test(
            expected_result_file_path=os.path.join(TESTS_RESOURCES_DIR_PATH, "query_inventory.json"),
            name=test_name,
            your_result=Query(database=database_api.database).inventory,
        )
    except NotImplementedError:
        logger.warning(f"🕒 TEST {test_name}: SOLUTION NOT IMPLEMENTED YET")
    # Class method models.query.Query.list
    test_name = "models.query.Query.list 1/4"
    try:
        run_test(
            expected_result_file_path=os.path.join(TESTS_RESOURCES_DIR_PATH, "query_list_writer.json"),
            name=test_name,
            your_result=Query(database=database_api.database).list(movie_detail="writer"),
        )
    except NotImplementedError:
        logger.warning(f"🕒 TEST {test_name}: SOLUTION NOT IMPLEMENTED YET")
    test_name = "models.query.Query.list 2/4"
    try:
        run_test(
            expected_result_file_path=os.path.join(TESTS_RESOURCES_DIR_PATH, "query_list_directors.json"),
            name=test_name,
            your_result=Query(database=database_api.database).list(movie_detail="directors"),
        )
    except NotImplementedError:
        logger.warning(f"🕒 TEST {test_name}: SOLUTION NOT IMPLEMENTED YET")
    test_name = "models.query.Query.list 3/4"
    try:
        run_test(
            expected_result_file_path=os.path.join(TESTS_RESOURCES_DIR_PATH, "query_list_director.json"),
            name=test_name,
            your_result=Query(database=database_api.database).list(movie_detail="director"),
        )
    except NotImplementedError:
        logger.warning(f"🕒 TEST {test_name}: SOLUTION NOT IMPLEMENTED YET")
    test_name = "models.query.Query.list 4/4"
    try:
        run_test(
            expected_result_file_path=os.path.join(TESTS_RESOURCES_DIR_PATH, "query_list_genre.json"),
            name=test_name,
            your_result=Query(database=database_api.database).list(movie_detail="genre"),
        )
    except NotImplementedError:
        logger.warning(f"🕒 TEST {test_name}: SOLUTION NOT IMPLEMENTED YET")
    # Class method models.query.Query.send
    test_name = "models.query.Query.send 1/12"
    try:
        run_test(
            expected_result_file_path=os.path.join(TESTS_RESOURCES_DIR_PATH, "query_send_title_words.json"),
            name=test_name,
            your_result=Query(
                database=database_api.database,
                title_words=[
                    "first",
                    "star",
                ],
            ).send(),
        )
    except NotImplementedError:
        logger.warning(f"🕒 TEST {test_name}: SOLUTION NOT IMPLEMENTED YET")
    test_name = "models.query.Query.send 2/12"
    try:
        run_test(
            expected_result_file_path=os.path.join(TESTS_RESOURCES_DIR_PATH, "query_send_director.json"),
            name=test_name,
            your_result=Query(
                database=database_api.database,
                director="Richard Donner",
            ).send(),
        )
    except NotImplementedError:
        logger.warning(f"🕒 TEST {test_name}: SOLUTION NOT IMPLEMENTED YET")
    test_name = "models.query.Query.send 3/12"
    try:
        run_test(
            expected_result_file_path=os.path.join(TESTS_RESOURCES_DIR_PATH, "query_send_actor_director.json"),
            name=test_name,
            your_result=Query(
                actor="Tom Cruise",
                database=database_api.database,
                director="Steven Spielberg",
            ).send(),
        )
    except NotImplementedError:
        logger.warning(f"🕒 TEST {test_name}: SOLUTION NOT IMPLEMENTED YET")
    test_name = "models.query.Query.send 4/12"
    try:
        run_test(
            expected_result_file_path=os.path.join(TESTS_RESOURCES_DIR_PATH, "query_send_actor_director.json"),
            name=test_name,
            your_result=Query(
                actor="Tom Cruise",
                database=database_api.database,
                director="Steven Spielberg",
            ).send(),
        )
    except NotImplementedError:
        logger.warning(f"🕒 TEST {test_name}: SOLUTION NOT IMPLEMENTED YET")
    test_name = "models.query.Query.send 5/12"
    try:
        run_test(
            expected_result_file_path=os.path.join(TESTS_RESOURCES_DIR_PATH, "query_send_composer_director.json"),
            name=test_name,
            your_result=Query(
                composer="John Williams",
                database=database_api.database,
                director="Steven Spielberg",
            ).send(),
        )
    except NotImplementedError:
        logger.warning(f"🕒 TEST {test_name}: SOLUTION NOT IMPLEMENTED YET")
    test_name = "models.query.Query.send 6/12"
    try:
        run_test(
            expected_result_file_path=os.path.join(TESTS_RESOURCES_DIR_PATH, "query_send_composer_director_year.json"),
            name=test_name,
            your_result=Query(
                composer="Jerry Goldsmith",
                database=database_api.database,
                director="Franklin J. Schaffner",
                year=1968,
            ).send(),
        )
    except NotImplementedError:
        logger.warning(f"🕒 TEST {test_name}: SOLUTION NOT IMPLEMENTED YET")
    test_name = "models.query.Query.send 7/12"
    try:
        run_test(
            expected_result_file_path=os.path.join(TESTS_RESOURCES_DIR_PATH, "query_send_composer_unknown.json"),
            name=test_name,
            your_result=Query(
                composer="Bernard Herrmann",
                database=database_api.database,
            ).send(),
        )
    except NotImplementedError:
        logger.warning(f"🕒 TEST {test_name}: SOLUTION NOT IMPLEMENTED YET")
    test_name = "models.query.Query.send 8/12"
    try:
        run_test(
            expected_result_file_path=os.path.join(TESTS_RESOURCES_DIR_PATH, "query_send_genre.json"),
            name=test_name,
            your_result=Query(
                database=database_api.database,
                genre="film_noir",
            ).send(),
        )
    except NotImplementedError:
        logger.warning(f"🕒 TEST {test_name}: SOLUTION NOT IMPLEMENTED YET")
    test_name = "models.query.Query.send 9/12"
    try:
        run_test(
            expected_result_file_path=os.path.join(TESTS_RESOURCES_DIR_PATH, "query_send_composer_genre.json"),
            name=test_name,
            your_result=Query(
                composer="John Williams",
                database=database_api.database,
                genre="western",
            ).send(),
        )
    except NotImplementedError:
        logger.warning(f"🕒 TEST {test_name}: SOLUTION NOT IMPLEMENTED YET")
    test_name = "models.query.Query.send 10/12"
    try:
        run_test(
            expected_result_file_path=os.path.join(TESTS_RESOURCES_DIR_PATH, "query_send_genre_unknown.json"),
            name=test_name,
            your_result=Query(
                database=database_api.database,
                genre="romance",
            ).send(),
        )
    except NotImplementedError:
        logger.warning(f"🕒 TEST {test_name}: SOLUTION NOT IMPLEMENTED YET")
    test_name = "models.query.Query.send 11/12"
    try:
        run_test(
            expected_result_file_path=os.path.join(TESTS_RESOURCES_DIR_PATH, "query_send_actor_composer_director_genre_year.json"),
            name=test_name,
            your_result=Query(
                actor="Julia Roberts",
                composer="John Williams",
                database=database_api.database,
                director="Steven Spielberg",
                genre="fantasy",
                year=1991,
            ).send(),
        )
    except NotImplementedError:
        logger.warning(f"🕒 TEST {test_name}: SOLUTION NOT IMPLEMENTED YET")
    test_name = "models.query.Query.send 12/12"
    try:
        run_test(
            expected_result_file_path=os.path.join(TESTS_RESOURCES_DIR_PATH, "query_send_year.json"),
            name=test_name,
            your_result=Query(
                database=database_api.database,
                year=1979,
            ).send(),
        )
    except NotImplementedError:
        logger.warning(f"🕒 TEST {test_name}: SOLUTION NOT IMPLEMENTED YET")


if __name__ == "__main__":
    # To run this script, use the following command with Python3.8:
    # $ python main.py
    sys.exit(my_tests())
