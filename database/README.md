# Purpose
This database contains JSON files descripting movies scored by American composers John Williams and Jerry Goldsmith

# Table of contents
* [Sorting](#sorting)
* [JSON files](#json-files)
* [Parameters](#parameters)
* [Authors](#authors)

# Sorting
All JSON files are sorting by movie genre (ex: horror, thriller...)

# JSON files
Each JSON file corresponds to a movie scored by John Williams or Jerry Goldsmith

# Parameters
Each JSON file features the following parameters:
- cast: Dict[str: int] = enum
- composer: str
- director: str
- year: int

# Authors
- [BURTSCHER Clément](https://gitlab.navya.tech/clement.burtscher)
