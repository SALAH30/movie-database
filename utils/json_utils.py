"""
Utilities for managing JSON files
"""
import json

from utils.logger import logger


class JSONReadError(Exception):
    """
    Raised if a JSON file can not be read
    """


class JSONFileCorruptedError(JSONReadError):
    """
    Raised if a JSON file is corrupted
    """

class JSONFileNotFoundError(JSONReadError):
    """
    Raised if a JSON file is does not exist
    """


class JSONUtils:
    """
    Class for parsing JSON files
    """
    @staticmethod
    def read_json(
        file_path: str,
        exit_on_error: bool=False,
    ) -> dict:
        """
        Read a JSON file and store its content in a Python dictionary

        :param bool exit_on_error: If True, raise error
        :param str file_path: The path to the JSON file

        :return: The JSON file content stored in a dictionary
        :rtype: dict
        """
        content = {}
        try:
            with open(file=file_path, mode="r+", encoding="utf-8") as file:
                try:
                    content = json.load(fp=file)
                except (json.JSONDecodeError, ValueError) as err:
                    error_msg = f"The file {file_path} is corrupted:\n{err}"
                    if exit_on_error:
                        raise JSONFileCorruptedError(error_msg)
                    else:
                        logger.error(error_msg)
        except FileNotFoundError:
            error_msg = f"The file {file_path} does not exist"
            if exit_on_error:
                raise JSONFileNotFoundError(error_msg)
            else:
                logger.error(error_msg)
        return content

    @staticmethod
    def write_json(content: dict, file_path: str) -> None:
        """
        Write the content of a Python dictionary into a JSON file

        :param dict content: The Python dictionary
        :param str file_path: The path to the JSON file

        :return: None
        """
        with open(file=file_path, mode="w+", encoding="utf-8") as file:
            json.dump(
                fp=file,
                indent=4,
                obj=content,
                sort_keys=False,
            )
