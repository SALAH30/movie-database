"""
Custom logger
"""
import logging


logger = logging.getLogger(name="database_cinema")
# By default the root logger is set to WARNING
# Set the root logger level to INFO
logger.setLevel(level=logging.INFO)
stream_handler = logging.StreamHandler()
stream_handler.setLevel(level=logging.DEBUG)
stream_handler.setFormatter(
    fmt=logging.Formatter(fmt="[%(asctime)s] [%(name)s] %(levelname)s [%(module)s:%(lineno)d:%(funcName)s] %(message)s"),
)
logger.addHandler(hdlr=stream_handler)
