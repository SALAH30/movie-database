"""
Global variables
"""
import os

MAIN_FILE_PATH: str = os.path.normpath(os.path.abspath(path=os.path.join(os.path.dirname(p=__file__), "..")))
"""
Path to the main.py file
"""
DATABASE_DIR_PATH: str = os.path.join(MAIN_FILE_PATH, "database")
"""
Path to the database directory
"""
TESTS_RESOURCES_DIR_PATH: str = os.path.join(MAIN_FILE_PATH, "tests", "resources")
"""
Path to the tests resources
"""