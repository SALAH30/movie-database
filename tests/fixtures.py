"""
Tests fixtures
"""
import os
import typing

from globals.globals import DATABASE_DIR_PATH

JSON_FILE_PATHS: typing.Dict[str, typing.List[str]] =  {
    "adventure": [
        os.path.join(DATABASE_DIR_PATH, "adventure", "Raiders_Of_The_Lost_Ark.json"),
        os.path.join(DATABASE_DIR_PATH, "adventure", "The_Mummy.json"),
    ],
    "fantasy": [
        os.path.join(DATABASE_DIR_PATH, "fantasy", "Harry_Potter_And_The_Philosophers_Stone.json"),
        os.path.join(DATABASE_DIR_PATH, "fantasy", "Hook.json"),
    ],
    "film_noir": [
        os.path.join(DATABASE_DIR_PATH, "film_noir", "Chinatown.json"),
        os.path.join(DATABASE_DIR_PATH, "film_noir", "LA_Confidential.json"),
        os.path.join(DATABASE_DIR_PATH, "film_noir", "The_Long_Goodbye.json"),
    ],
    "historical": [
        os.path.join(DATABASE_DIR_PATH, "historical", "JFK.json"),
    ],
    "horror": [
        os.path.join(DATABASE_DIR_PATH, "horror", "Dracula.json"),
        os.path.join(DATABASE_DIR_PATH, "horror", "Gremlins.json"),
        os.path.join(DATABASE_DIR_PATH, "horror", "Jaws.json"),
        os.path.join(DATABASE_DIR_PATH, "horror", "Poltergeist.json"),
        os.path.join(DATABASE_DIR_PATH, "horror", "The_Omen.json"),
    ],
    "medieval": [
        os.path.join(DATABASE_DIR_PATH, "medieval", "First_Knight.json"),
    ],
    "science_fiction": [
        os.path.join(DATABASE_DIR_PATH, "science_fiction", "Alien.json"),
        os.path.join(DATABASE_DIR_PATH, "science_fiction", "ET_The_Extra_Terrestrial.json"),
        os.path.join(DATABASE_DIR_PATH, "science_fiction", "Jurassic_Park.json"),
        os.path.join(DATABASE_DIR_PATH, "science_fiction", "Minority_Report.json"),
        os.path.join(DATABASE_DIR_PATH, "science_fiction", "Planet_Of_The_Apes.json"),
        os.path.join(DATABASE_DIR_PATH, "science_fiction", "Star_Trek_The_Motion_Picture.json"),
        os.path.join(DATABASE_DIR_PATH, "science_fiction", "Star_Wars_Episode_IV_A_New_Hope.json"),
    ],
    "superhero": [
        os.path.join(DATABASE_DIR_PATH, "superhero", "Supergirl.json"),
        os.path.join(DATABASE_DIR_PATH, "superhero", "Superman.json"),
    ],
    "thriller": [
        os.path.join(DATABASE_DIR_PATH, "thriller", "Basic_Instinct.json"),
        os.path.join(DATABASE_DIR_PATH, "thriller", "Presumed_Innocent.json"),
    ],
    "war": [
        os.path.join(DATABASE_DIR_PATH, "war", "Empire_Of_The_Sun.json"),
        os.path.join(DATABASE_DIR_PATH, "war", "First_Blood.json"),
        os.path.join(DATABASE_DIR_PATH, "war", "Patton.json"),
        os.path.join(DATABASE_DIR_PATH, "war", "Saving_Private_Ryan.json"),
    ],
    "western": [
        os.path.join(DATABASE_DIR_PATH, "western", "Lonely_Are_The_Brave.json"),
        os.path.join(DATABASE_DIR_PATH, "western", "The_Man_Who_Loved_Cat_Dancing.json"),
        os.path.join(DATABASE_DIR_PATH, "western", "The_Missouri_Breaks.json"),
        os.path.join(DATABASE_DIR_PATH, "western", "Wild_Rovers.json"),
    ],
}
"""
JSON file paths sorted by movie genre = database subdirectories
"""
