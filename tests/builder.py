"""
Build tests resources
"""
from utils.json_utils import JSONUtils



class TestsBuilder:
    """
    A class for building tests resources
    """
    @staticmethod
    def dump_fixture(
        fixture: dict,
        file_path: str,
    ) -> None:
        """
        Dump dictionary fixture to JSON file

        :param dict fixture: The dictionary fixture to dump
        :param str file_path: The output JSON file path

        :return: None
        """
        JSONUtils.write_json(
            content=fixture,
            file_path=file_path,
        )
