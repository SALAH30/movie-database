"""
Validator for tests
"""
import json


class Validator:
    """
    A class for assertion tests
    """
    @staticmethod
    def diff_dict(ref_dict: dict, comp_dict: dict) -> str:
        """
        Compare 2 dictionaries (ex: loaded JSON files)

        :param dict ref_dict: The reference dictionary
        :param dict comp_dict: The dictionary to compare with

        :return: Error message
        :rtype: str
        """
        error_msg = ""
        if isinstance(comp_dict, dict):
            for key in ref_dict.keys():
                try:
                    if comp_dict[key] != ref_dict[key]:
                        error_msg = \
                            f"⚠️  Keys {key} mismatch ⚠️\n" \
                            f"your_result =\n{json.dumps(obj={key: comp_dict[key]}, indent=4)}\n" \
                            f"expected_result =\n{json.dumps(obj={key: ref_dict[key]}, indent=4)}"
                        break
                except KeyError:
                    error_msg = \
                        f"⚠️  Missing key {key} ⚠️\n" \
                        f"your_result =\n{json.dumps(obj=comp_dict, indent=4)}"
                    break
        else:
            error_msg = \
                f"⚠️  Not of type dict ⚠️\n" \
                f"your_result =\n{json.dumps(obj=comp_dict, indent=4)}"
        return error_msg
