"""
Query the JSON database
"""
import typing

from utils.logger import logger


class MovieTitleContainsOneOfSearchedWords(Exception):
    """
    Exception to raise if a movie title contains at least one of the searched words
    """
    pass

class Query:
    """
    A class for implementing queries
    """
    def __init__(self,
        actor: str=None,
        composer: str=None,
        database: dict=None,
        director: str=None,
        genre: str=None,
        title_words: typing.List[str]=None,
        year: int=None,
    ) -> None:
        """
        :param str actor: The actor from the movies to find
        :param str composer: The composer from the movies to find
        :param str database: The JSON database loaded as dictionary
        :param str director: The director from the movies to find
        :param str genre: The genre from the movies to find
        :param str title_words: The words present in the titles from the movies to find
        :param int year: The release year from the movies to find

        :return: None
        """
        # Check the integrity of supplied input parameters
        self.actor = actor
        self.composer = composer
        self.database = database
        self.director = director
        self.genre = genre
        self.title_words = title_words
        self.year = year
        # Build the inventory from the database
        self._inventory = None

    # Getters
        
    @property
    def actor(self) -> str:
        """
        Get parameter actor

        :return: Parameter actor
        :rtype: str
        """
        return self._actor

    @property
    def composer(self) -> str:
        """
        Get parameter composer

        :return: Parameter composer
        :rtype: str
        """
        return self._composer

    @property
    def database(self) -> dict:
        """
        Get parameter database

        :return: Parameter database
        :rtype: dict
        """
        return self._database

    @property
    def director(self) -> str:
        """
        Get parameter director

        :return: Parameter director
        :rtype: str
        """
        return self._director

    @property
    def genre(self) -> str:
        """
        Get parameter genre

        :return: Parameter genre
        :rtype: str
        """
        return self._genre

    @property
    def inventory(self) -> typing.Dict[str, typing.List[str]]:
        """
        Get parameter inventory

        :return: Parameter inventory
        :rtype: str
        """
        if self._inventory is None:
            self._inventory = {}
            #########################
            for details in self.database.values():
                for k, v in details.items():
                    if k not in self._inventory:
                        self._inventory[k] = []
                for key, value in details.items():
                    if key == "cast":
                        for v in value.keys():
                            if v not in self._inventory[key]:
                                self._inventory[key].append(v)
                    else:
                        if value not in self._inventory[key]:
                            self._inventory[key].append(value)
        self._inventory = {key: sorted(value) for key, value in sorted(self._inventory.items())}
            #########################
        return self._inventory

    @property
    def title_words(self) -> typing.List[str]:
        """
        Get parameter title_words

        :return: Parameter title_words
        :rtype: typing.List[str]
        """
        return self._title_words

    @property
    def year(self) -> int:
        """
        Get parameter year

        :return: Parameter year
        :rtype: int
        """
        return self._year

    # Setters

    @actor.setter
    def actor(self, value: str) -> None:
        """
        Set parameter actor

        :param str value: Value to set

        :return: None
        """
        self._actor = None
        if value is not None:
            if isinstance(value, str):
                self._actor = value
            else:
                logger.error(f"Type of value = {value} is {type(value).__name__} but must be str, it will be set to default = {self._actor}")

    @composer.setter
    def composer(self, value: str) -> None:
        """
        Set parameter composer

        :param str value: Value to set

        :return: None
        """
        self._composer = None
        if value is not None:
            if isinstance(value, str):
                self._composer = value
            else:
                logger.error(f"Type of value = {value} is {type(value).__name__} but must be str, it will be set to default = {self._composer}")

    @database.setter
    def database(self, value: dict) -> None:
        """
        Set parameter database

        :param dict value: Value to set

        :return: None
        """
        self._database = None
        if value is not None:
            if isinstance(value, dict):
                self._database = value
            else:
                logger.error(f"Type of value = {value} is {type(value).__name__} but must be dict, it will be set to default = {self._database}")

    @director.setter
    def director(self, value: str) -> None:
        """
        Set parameter director

        :param str value: Value to set

        :return: None
        """
        self._director = None
        if value is not None:
            if isinstance(value, str):
                self._director = value
            else:
                logger.error(f"Type of value = {value} is {type(value).__name__} but must be str, it will be set to default = {self._director}")

    @genre.setter
    def genre(self, value: str) -> None:
        """
        Set parameter genre

        :param str value: Value to set

        :return: None
        """
        self._genre = None
        if value is not None:
            if isinstance(value, str):
                self._genre = value
            else:
                logger.error(f"Type of value = {value} is {type(value).__name__} but must be str, it will be set to default = {self._genre}")

    @title_words.setter
    def title_words(self, value: typing.List[str]) -> None:
        """
        Set parameter title_words

        :param typing.List[str] value: Value to set

        :return: None
        """
        self._title_words = []
        if value is not None:
            if isinstance(value, list):
                for word in value:
                    if isinstance(word, str):
                        self._title_words += [word.lower()]
                    else:
                        logger.error(f"Type of word = {word} is {type(value).__name__} but must be str, it will be ignored")
                self._title_words = sorted(self._title_words)
            else:
                logger.error(f"Type of value = {value} is {type(value).__name__} but must be typing.List[str], it will be set to default = {self._title_words}")

    @year.setter
    def year(self, value: int) -> None:
        """
        Set parameter year

        :param int value: Value to set

        :return: None
        """
        self._year = None
        if value is not None:
            if isinstance(value, int):
                self._year = value
            else:
                logger.error(f"Type of value = {value} is {type(value).__name__} but must be int, it will be set to default = {self._year}")

    # Class methods

    def list(self, movie_detail: str) -> dict:
        """
        List all values featured in the database for the supplied movie detail (ex: list all genres if movie_detail = "genre")

        :param str movie_detail: The movie detail to consider

        :return: The list of values featured in the database for the supplied movie detail
        :rtype: dict
        """
        answer = {movie_detail: []}
        for movie, details in self.database.items():
            if movie_detail in details.keys():
                if details[movie_detail] not in answer[movie_detail]:
                    answer[movie_detail].append(details[movie_detail])

        answer = {key: sorted(value) for key, value in sorted(answer.items())}
        return answer

    def send(self) -> dict:
        """
        Send the query to the JSON database

        :return: The criteria and result of the query
        :rtype: dict
        """
        answer = {
            "criteria": {
                "actor": self.actor,
                "composer": self.composer,
                "director": self.director,
                "genre": self.genre,
                "title_words": self.title_words,
                "year": self.year,
            },
            "result": []
        }
        #########################
        for movie, details in self.database.items():
            for criteria, value in answer["criteria"].items():
                exists = True
                if criteria == "actor":
                    if value not in list(details["cast"].keys()):
                        exists = False
                elif criteria == "title_words":
                    for word in value:
                        if not movie.find(word):
                            # raise MovieTitleContainsOneOfSearchedWords()
                        # else:
                            exists = False
                else:
                    if details[criteria] != value:
                        exists = False
                
            if exists:
                answer["result"].append(movie)
        answer = {key: sorted(value) for key, value in sorted(answer.items())}
        #########################
        return answer
