"""
Browse JSON database
"""
import json
import os
import typing


from utils.logger import logger


class DatabaseApi:
    """
    A class for browsing the JSON database
    """
    def __init__(self,
        database_dir_path: str,
    ) -> None:
        """
        :param str database_dir_path: The path to the JSON database directory, containing subdirectories = movie genres

        :return: None
        """
        # Check the integrity of the path to the JSON database directory
        self.database_dir_path = database_dir_path
        # Retrieve the paths to the database JSON files
        self._json_file_paths = None
        # Load all JSON files as a sorted Python dictionary
        self._database = None

    # Getters

    @property
    def database(self) -> dict:
        """
        Get parameter database

        :return: Parameter database
        :rtype: dict
        """
        self._database = {}
        if self.json_file_paths is not None:
            #########################
            for genre, movie_names in self._json_file_paths.items():
                for movie in movie_names:
                    # with open(movie, "r") as file:
                    with open(movie) as f:
                        data = json.load(f)
                        data[movie.split("/")[-1].split(".")[0]]["genre"] = genre
                        self._database[movie.split("/")[-1].split(".")[0]] = data[movie.split("/")[-1].split(".")[0]]

            #########################
        return self._database

    @property
    def database_dir_path(self) -> str:
        """
        Get parameter database_dir_path

        :return: Parameter database_dir_path
        :rtype: str
        """
        return self._database_dir_path

    @property
    def json_file_paths(self) -> typing.Dict[str, str]:
        """
        Get parameter json_file_paths

        :return: Parameter json_file_paths
        :rtype: typing.Dict[str, str]
        """
        self._json_file_paths = {}
        if self.database_dir_path is not None:
            for filename in os.listdir(self.database_dir_path):
                f = os.path.join(self.database_dir_path, filename)
                # checking if it is a file
                if not os.path.isfile(f):
                    self._json_file_paths[f.split("/")[-1]] = []
            for key in self._json_file_paths.keys():
                for filename in os.listdir(self.database_dir_path+"/"+key):
                    f = os.path.join(self.database_dir_path+"/"+key, filename)
                    if os.path.isfile(f):
                        if f.split(".")[-1] == "json":
                            self._json_file_paths[key].append(f)
        # sorting
        self._json_file_paths = {key: sorted(value) for key, value in sorted(self._json_file_paths.items())}
        return self._json_file_paths
    
    # Setters

    @database_dir_path.setter
    def database_dir_path(self, value: str) -> None:
        """
        Set parameter database_dir_path

        :param str value: Value to set

        :return: None
        """
        self._database_dir_path = None
        if value is not None:
            if isinstance(value, str):
                if os.path.isdir(s=value):
                    self._database_dir_path = os.path.normpath(path=value)
                    logger.info(f"The path to the database directory is:\n{self._database_dir_path}")
                else:
                    logger.error(f"Path {value} is not a directory, it will be set to default = {self._database_dir_path}")
            else:
                logger.error(f"Type of value = {value} is {type(value).__name__} but must be str, it will be set to default = {self._database_dir_path}")
